package data

class Operation (
    val firstNumberInput: String,
    val secondNumberInput: String,
    val operator: String
) {
    /**
     * The strings interpolation allows us to build
     * strings in an easy way.
     * In this example we're using the value directly using $value
     * if we're going to add the value of an object we need to wrap them like this:
     * "My name is ${person.name}"
     */
    override fun toString(): String {
        return "First Number: $firstNumberInput \nSecond Number: $secondNumberInput \nOperator: $operator"
    }
}