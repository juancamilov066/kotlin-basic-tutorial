package data

/**
 * Data classes act as POJOs of Java.
 */
data class OperationResult (
    val result: Number?,
    val error: String?
)