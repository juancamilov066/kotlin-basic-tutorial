/**
 * LIST: An ordered collection where their elements
 * can be present more than once. They have an index
 * which represent a position of any of their children
 */

/**
 * The List interface is immutable by default
 * We only have read access only to their values.
 */
private var a: List<Int> = listOf(1, 3, 4)
private var c: ArrayList<Int> = ArrayList()

/**
 * The MutableList interface gives us access to read and
 * write their children.
 */
private var b: MutableList<Int> = mutableListOf(1, 4, 6)


/**
 * SET: A collection that doesn't accept repeated elements.
 * Despite we can access to their items trough indexes, the order
 * doesn't matters too much.
 */

/**
 * The same as lists, they're immutable by default
 */
private var d: Set<Int> = setOf(1, 2, 3, 4)
private var e: MutableSet<Int> = mutableSetOf(1, 2, 3 , 4)

/**
 * MAP: Key-Value pairs. The keys must be unique, the values can be the same
 * for each key.
 */

/**
 * Over again, they're immutable by default
 */
private var g: Map<Int, String> = mapOf(Pair(1, "a"), Pair(2, "b"))
private var f: MutableMap<Int, String> = mutableMapOf(Pair(1, "a"), Pair(2, "b"))
