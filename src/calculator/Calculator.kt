package calculator

import data.Operation
import data.OperationResult

interface Calculator {
    fun calculate(operation: Operation): OperationResult
    fun validate(operation: Operation): Boolean
    fun printOperationsHistory()
    fun saveOperation(operation: Operation)
}