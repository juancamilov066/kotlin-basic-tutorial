package calculator.impl

import calculator.Calculator
import calculator.enums.Operations
import data.Operation
import data.OperationResult
import utils.Validator

class CalculatorImpl : Calculator {

    private val operations: ArrayList<Operation> by lazy { ArrayList<Operation>() }
    //lateinit var operations: ArrayList<Operation>

    /*fun init() {
        operations = ArrayList()
    }*/

    private fun ArrayList<Operation>.printOperations() = if (this.size > 0) {
        this.forEachIndexed { index, operation ->
            println("Operation ${index + 1}: $operation")
        }
    } else {
        println("·---- YOU DON'T HAVE ADDED ANY OPERATION YET ----·")
    }

    override fun calculate(operation: Operation): OperationResult {
        if (validate(operation)) {
            when (operation.operator) {
                Operations.PLUS.operation -> {
                    saveOperation(operation)
                    return OperationResult(
                        operation.firstNumberInput.toDouble() + operation.secondNumberInput.toDouble(),
                        null
                    )
                }
                Operations.MINUS.operation -> {
                    saveOperation(operation)
                    return OperationResult(
                        operation.firstNumberInput.toDouble() - operation.secondNumberInput.toDouble(),
                        null
                    )
                }
                Operations.OBELUS.operation -> {
                    saveOperation(operation)
                    return OperationResult(
                        operation.firstNumberInput.toDouble() / operation.secondNumberInput.toDouble(),
                        null
                    )
                }
                Operations.TIMES.operation -> {
                    saveOperation(operation)
                    return OperationResult(
                        operation.firstNumberInput.toDouble() * operation.secondNumberInput.toDouble(),
                        null
                    )
                }
                else -> { return OperationResult(null, "You've entered an unknown operator") }
            }
        } else {
            return OperationResult(null, "The operation you've entered is wrong")
        }
    }

    override fun validate(operation: Operation): Boolean {
        return Validator.isOperationValid(operation)
    }

    override fun printOperationsHistory() {
        operations.printOperations()
    }

    override fun saveOperation(operation: Operation) {
        operations.add(operation)
    }
}