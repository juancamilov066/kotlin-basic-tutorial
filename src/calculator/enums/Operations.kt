package calculator.enums

enum class Operations (val operation: String) {
    PLUS("+"),
    MINUS("-"),
    OBELUS("/"),
    TIMES("x")
}