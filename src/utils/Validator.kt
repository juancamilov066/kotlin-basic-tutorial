package utils

import calculator.enums.Operations
import data.Operation
import java.lang.NumberFormatException

/**
 * Is how Kotlin generates singleton classes.
 * Their lifecycle lives across the app execution. It's very similar to
 * the static voids on Java
 */
object Validator {

    fun isOperationValid (operation: Operation): Boolean {
        return isOperatorValid(operation.operator)
                && isNumber(operation.firstNumberInput)
                && isNumber(operation.secondNumberInput)
    }

    
    private fun isOperatorValid(operator: String): Boolean {
        return when (operator) {
            Operations.PLUS.operation -> true
            Operations.MINUS.operation -> true
            Operations.OBELUS.operation -> true
            Operations.TIMES.operation -> true
            else -> false
        }
    }

    /**
     * Here we can see the simplicity that Kotlin adds
     * to our code.
     * We can return the try declaration directly if we want to
     */
    private fun isNumber(numberInput: String): Boolean {
        var isNumber = false
        isNumber = try {
            numberInput.toDouble()
            true
        } catch (e: NumberFormatException) {
            false
        }
        /*return try {
            val number = numberInput.toDouble()
            true
        } catch (e: NumberFormatException) {
            false
        }*/
        return isNumber
    }
}