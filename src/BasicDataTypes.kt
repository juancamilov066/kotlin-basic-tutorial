private var a: Double = 2.0
private var b: Float = 3F
private var c: String = "Hello World"
private var d: Boolean = true
private var f: Int = 1
private var g: Byte = 2 //16 bit width
private var h: Long = 47568975869708 //64 bit width
private var i: Short = 1 //16 bit width