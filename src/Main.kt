import calculator.Calculator
import data.Operation
import data.OperationResult

/**
 * One of the kotlin advantages is that
 * we don't need to declare functions inside
 * a class in order to use them
 */
fun main(args : Array<String>) {
    val calculator: Calculator = calculator.impl.CalculatorImpl()

    var execute = 1
    while (execute == 1) {
        println("Welcome to the most basic calculator of the world")
        println("·---- MENU ----·\nEnter 1 to start.\nEnter 2 to see the operation list.\nEnter 0 to exit.")

        /**
         * With the !! operator we tell to the compiler that
         * we're sure that the returned value will never be null
         */
        when (readLine()!!) {
            "1" -> {
                println("\n--- ·· Enter the first number ·· ---")
                val firstNumber = readLine()!!
                println("\n--- ·· Great, enter the second number ·· ---")
                val secondNumber = readLine()!!
                println("\n--- ·· What do you want to do with $firstNumber and $secondNumber? ·· ---")
                println("\n\n--- ·· CHEAT SHEET ·· ---")
                println("· SUM (Enter +)")
                println("· MINUS (Enter -)")
                println("· OBELUS (Enter /)")
                println("· TIMES (Enter x)")
                val operator = readLine()!!

                val operation = Operation(firstNumber, secondNumber, operator)
                val result: OperationResult = calculator.calculate(operation)

                if (result.error == null && result.result != null) {
                    println("The operation result is: ${result.result}")
                } else {
                    println("Error: ${result.error}")
                }
            }
            "2" -> {
                calculator.printOperationsHistory()
            }
            "0" -> {
                execute = 0
            }
            else -> println("· Unknown option ·")
        }
    }
}